# WebMalletMVC

Web application to use Java-based package for statistical natural language processing MALLET. An MVC Java-based application with Spring Boot (whith includes Spring MVC) and Hibernate based on a MySQL database.
More information about MALLET (MAchine Learning for LanguagE Toolkit) can be found at http://mallet.cs.umass.edu/

Applicazione web per l'utilizzo del pacchetto Java per l'analisi statistica del linguaggio naturale MALLET. Un'applicazione MVC in Java con Spring Boot (che include Spring MVC) ed Hibernate basata su un database MySQL.
Per ulteriori informazioni su MALLET (MAchine Learning for LanguagE Toolkit) vedere http://mallet.cs.umass.edu/